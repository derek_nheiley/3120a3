#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>

#define X_INSTRUCTIONS_NOT_NEEDED
#include "xis.h"
#include "xcpu.h"

#define TICK_ARG 1
#define IMAGE_ARG 2
#define QUANTUM_ARG 3
#define CPUS_ARG 4

int ticks;
int quantum;

void* run(xcpu * cs){
  unsigned int i;
  for( i = 0; ( ticks < 1 ) || ( i < ticks ); i++ ) {
    if( i && ( quantum > 0 ) && !( i % quantum ) ) {
      if( !xcpu_exception( cs, X_E_INTR ) ) {
        fprintf( stderr, "Exception error, CPU %d has halted.\n", cs->id);
        return 0;
      } 
    }
    if ( !xcpu_execute( cs ) ) {
      fprintf( stderr, "CPU %d has halted.\n", cs->id );
      return 0;
    }
  }
  fprintf( stderr, "CPU %d ran out of time.\n", cs->id );
  return 0;
}

int main( int argc, char **argv ) {

  FILE *fp;
  struct stat fs;
  unsigned char *mem;
  int cpus;
  int p;
  xcpu *cs; //array of p cpus
  pthread_t * tid; //array of p threads

  if( ( argc < 4 ) || ( sscanf( argv[TICK_ARG], "%d", &ticks ) != 1 ) || 
      ( ticks < 0 ) || ( sscanf( argv[QUANTUM_ARG], "%d", &quantum ) != 1 ) ||
      ( quantum < 0 ) || ( sscanf( argv[CPUS_ARG], "%d", &cpus ) != 1 ) ||
      ( cpus < 1 ) || ( cpus > 32 ) ) {
    fprintf( stderr, "usage: xsim <ticks> <obj file> <quantum> <cpus>\n" );
    fprintf( stderr, 
            "      <ticks> is number instructions to execute (0 = forever)\n" );
    fprintf( stderr, 
            "      <image file> xis object file created by or xasxld\n" );
    fprintf( stderr, 
            "      <quantum> is the # of ticks) between interrupts %s\n",
            "(0 = no interrupts)" );
    fprintf( stderr, 
            "      <cpus> is the # of cpu threads (min 1, max 32)\n" );
    return 1;
  } 

  mem = (unsigned char *)malloc( XIS_MEM_SIZE );
  if( !mem ) {
    fprintf( stderr, "error: memory allocation (%d) failed\n", XIS_MEM_SIZE );
    exit( 1 );
  }
  memset( mem, I_BAD, XIS_MEM_SIZE );

  if( stat( argv[IMAGE_ARG], &fs ) ) {
    fprintf( stderr, "error: could not stat image file %s\n", argv[IMAGE_ARG] );
    return 1;
  } else if( fs.st_size > XIS_MEM_SIZE ) {
    fprintf( stderr, "Not enough memory to run all the programs." );
    return 1;
  }

  fp = fopen( argv[IMAGE_ARG], "rb" );
  if( !fp ) {
    fprintf( stderr, "error: could not open image file %s\n", argv[IMAGE_ARG] );
    return 1;
  } else if( fread( mem, 1, fs.st_size, fp ) != fs.st_size ) {
    fprintf( stderr, "error: could not read file %s\n", argv[IMAGE_ARG] );
    return 1;
  }
  fclose( fp );

  cs = (void *) malloc( cpus * sizeof(xcpu));
  for( p=cpus-1; p>=0; p-- ){
    memset( &cs[p], 0, sizeof( xcpu ) );
    cs[p].memory = mem;
    cs[p].id = p;
    cs[p].num = cpus;
  }

  // create the threads and pass reference to new shared run method
  tid = (pthread_t *) malloc( cpus * sizeof(pthread_t));
  for( p=cpus-1; p>=0; p--){ 
    if ( pthread_create( &(tid[p] ), NULL, &run, &(cs[p]) )!= 0 ) {
      fprintf( stderr, "error: could not create thread for CPU %d.\n", p );
    }
  }

  //wait for all the cpus to finish
  for( cpus--; cpus>=0; cpus--){ 
    pthread_join(tid[cpus], NULL);
  }

  free(cs);
  free(mem);

  return 0;
}


